<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresas = Empresa::all(); //obtiene todos los datos
        return response()->json($empresas);

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $empresa = Empresa::create($request->post());
        return response()->json([
            'empresa'=>$empresa
        ]);
    }

    public function show(Empresa $empresa)
    {
        return response()->json($empresa);
    }

    public function update(Request $request, Empresa $empresa)
    {
        $empresa->fill($request->post())->save();
        return response()->json([
            'empresa'=>$empresa
        ]);
    }

    public function destroy(Empresa $empresa)
    {
        $empresa->delete();
        return response()->json([
            'mensaje'=>'Empresa eliminada'
        ]);
    }
}
