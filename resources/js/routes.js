const Home = ()=> import('./components/Home.vue');

//importar los componenentes para empresa
const Mostrar = ()=> import('./components/empresa/Mostrar.vue');
const Crear = ()=> import('./components/empresa/Crear.vue');
const Editar = ()=> import('./components/empresa/Editar.vue');
const Filtar = ()=> import('./components/empresa/Filtrar.vue');

export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'mostrarEmpresas',
        path: '/empresas',
        component: Mostrar
    },
    {
        name: 'filtrar',
        path: '/filtrar',
        component: Filtar
    },
    {
        name: 'crearEmpresa',
        path: '/crear',
        component: Crear
    },
    {
        name: 'editarEmpresa',
        path: '/editar/:id',
        component: Editar
    }

];