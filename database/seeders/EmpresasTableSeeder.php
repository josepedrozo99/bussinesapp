<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Empresa;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa = new Empresa();
        $empresa->ruc = "123456-1";
        $empresa->nombre = "Empresa 1";
        $empresa->ciudad = "San Lorenzo";
        $empresa->save();

        $empresa = new Empresa();
        $empresa->ruc = "5512523";
        $empresa->nombre = "Lionel Messi";
        $empresa->ciudad = "Buenos Aires";
        $empresa->save();

        $empresa = new Empresa();
        $empresa->ruc = "654321-0";
        $empresa->nombre = "Empresa 2";
        $empresa->ciudad = "Asunción";
        $empresa->save();
    }
}
